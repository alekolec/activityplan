package com.alekolec.activityPlan.entity;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Sportsman {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String login;
	private String password;
	private String name;
	private String lastname;
	private LocalDate birthDate;
	@ManyToMany (fetch = FetchType.EAGER, mappedBy = "sportsmen")
	private Set<Training> trainings;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
	private Set<Training> ownedTrainings;
		
	public Sportsman() {}
		
	public Sportsman(long id, String login, String password, String name, String lastname) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.name = name;
		this.lastname = lastname;
	
	}
	
	
	public Sportsman(String login, String password, String name, String lastname) {
		super();
		this.login = login;
		this.password = password;
		this.name = name;
		this.lastname = lastname;

	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public Set<Training> getTrainings() {
		return trainings;
	}
	
	public void setTrainings(Set<Training> trainings) {
		this.trainings = trainings;
	}
	
	public Set<Training> getOwnedTrainings() {
		return ownedTrainings;
	}
	
	public void setOwnedTrainings(Set<Training> ownedTrainings) {
		this.ownedTrainings = ownedTrainings;
	}
	
	public LocalDate getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sportsman other = (Sportsman) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	
	
}
