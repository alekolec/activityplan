package com.alekolec.activityPlan.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Training {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	@ManyToMany (fetch = FetchType.EAGER)
	private Set<Sportsman> sportsmen;
	@ManyToOne
	private Activity activity;
	@ManyToOne
	private Place place;
	private LocalDateTime date;
	@ManyToOne
	private Sportsman owner;
	
	public Training() {}

	public Training(Set<Sportsman> sportsmen, Activity activity, LocalDateTime date) {
		super();
		this.sportsmen = sportsmen;
		this.activity = activity;
		this.date = date;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setSportsmen(Set<Sportsman> sportsmen) {
		this.sportsmen = sportsmen;
	}
	
	public Set<Sportsman> getSportsmen() {
		return sportsmen;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	public Place getPlace() {
		return place;
	}
	
	public void setPlace(Place place) {
		this.place = place;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Training other = (Training) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public Sportsman getOwner() {
		return owner;
	}
	
	public void setOwner(Sportsman owner) {
		this.owner = owner;
	}

	
	

}
