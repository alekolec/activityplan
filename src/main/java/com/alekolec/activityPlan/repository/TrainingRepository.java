package com.alekolec.activityPlan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.alekolec.activityPlan.entity.Training;

public interface TrainingRepository extends CrudRepository<Training, Long>{
	
	/* @Query("select t from Training t where t.place.id = ?1 and t.activity.id = ?2")
	  List<Training> findByPlaceAndActivity(long placeId, long activityId);*/
	 
	 @Query("select t from Training t where t.place.id = :placeId and t.activity.id = :activityId order by t.date ASC")
	  List<Training> findByPlaceAndActivity(@Param("placeId") long placeId, @Param("activityId") long activityId);
	 
	 @Query("select t from Training t where t not in (select tu from Training tu join tu.sportsmen s where s.id = :sportsmanId) and t.date > current_date order by t.date ASC")
	  List<Training> findTrainingsToJoin(@Param("sportsmanId") long sportsmanId);
	 
	 @Query("select t from Training t join t.sportsmen s where s.id = :sportsmanId order by t.date ASC")
	  List<Training> findJoinedTrainings(@Param("sportsmanId") long sportsmanId);
	 
	 @Query("select t from Training t where t.owner.id = :sportsmanId order by t.date ASC")
	  List<Training> findOwnedTrainings(@Param("sportsmanId") long sportsmanId);
	 
	

}
