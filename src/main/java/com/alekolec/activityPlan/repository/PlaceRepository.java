package com.alekolec.activityPlan.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.alekolec.activityPlan.entity.Place;

@Repository
public interface PlaceRepository extends CrudRepository<Place, Long> {

}
