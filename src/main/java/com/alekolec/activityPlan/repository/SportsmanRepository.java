package com.alekolec.activityPlan.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.alekolec.activityPlan.entity.Sportsman;

@Repository
public interface SportsmanRepository extends CrudRepository<Sportsman, Long>{

}
