package com.alekolec.activityPlan.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.alekolec.activityPlan.entity.Activity;

@Repository
public interface ActivityRepository extends CrudRepository<Activity, Long>{
	

}
