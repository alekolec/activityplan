package com.alekolec.activityPlan.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alekolec.activityPlan.entity.Activity;
import com.alekolec.activityPlan.entity.Place;
import com.alekolec.activityPlan.entity.Sportsman;
import com.alekolec.activityPlan.entity.Training;
import com.alekolec.activityPlan.repository.ActivityRepository;
import com.alekolec.activityPlan.repository.PlaceRepository;
import com.alekolec.activityPlan.repository.SportsmanRepository;
import com.alekolec.activityPlan.repository.TrainingRepository;

@Controller
@RequestMapping("/training")
public class TrainingController {

	@Autowired
	ActivityRepository activityRepository;

	@Autowired
	TrainingRepository trainingRepository;

	@Autowired
	SportsmanRepository sportsmanRepository;

	@Autowired
	PlaceRepository placeRepository;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addTrainingView(ModelMap model, HttpServletRequest request) {
		List<Activity> listOfActivities = (List<Activity>) activityRepository.findAll();
		List<Sportsman> listOfSportsmen = (List<Sportsman>) sportsmanRepository.findAll();
		List<Place> listOfPlaces = (List<Place>) placeRepository.findAll();
		
		List<Integer> listOfYear = new ArrayList<>();
		
		Integer now = LocalDate.now().getYear();
		for(int i= now; i<(now+10); i++) {
			listOfYear.add(i);
		}
		
		List<Integer> listOfMonths = new ArrayList<>();
		for(int i=1; i<=12; i++) {
			listOfMonths.add(i);
		}
		
		List<Integer> listOfDays = new ArrayList<>();
		for(int i=1; i<=31; i++) {
			listOfDays.add(i);
		}
		
		List<Integer> listOfHours = new ArrayList<>();
		for(int i=1; i<=24; i++) {
			listOfHours.add(i);
		}
		
		List<Integer> listOfMinutes = new ArrayList<>();
		for(int i=0; i<=45; i+=15) {
			listOfMinutes.add(i);
		}
		
		if (request.getSession() != null &&
				request.getSession().getAttribute("loggedin") != null &&
				request.getSession().getAttribute("loggedin").equals(true)) {
			Sportsman loggedSportsman = sportsmanRepository
					.findOne((Long) request.getSession().getAttribute("loggedId"));
			model.addAttribute("loggedSportsman", loggedSportsman);
		}

		model.addAttribute("places", listOfPlaces);
		model.addAttribute("activities", listOfActivities);
		model.addAttribute("sportsmen", listOfSportsmen);
		model.addAttribute("years", listOfYear);
		model.addAttribute("months", listOfMonths);
		model.addAttribute("days", listOfDays);
		model.addAttribute("hours", listOfHours);
		model.addAttribute("minutes", listOfMinutes);

		return "training/add";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addTraining(@RequestParam("activity") long activityId, @RequestParam("place") long placeId,
			@RequestParam("year") int year, @RequestParam("month") int month, @RequestParam("day") int day,
			@RequestParam("hour") int hour, @RequestParam("minute") int minute, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, RedirectAttributes redirectAttrs) throws IOException {
		String message;
		if (!Validator.validateDate(year, month, day) || !Validator.validateTime(hour, minute)) {
			message = "Podaj poprawną datę i godzinę ";
			
			redirectAttrs.addFlashAttribute("msg", message);
			return "redirect:/training/add";

		} else {
			Training training = new Training();
			training.setDate(LocalDateTime.of(year, month, day, hour, minute));
			training.setActivity(activityRepository.findOne(activityId));
			training.setPlace(placeRepository.findOne(placeId));

			Set<Sportsman> setOfSportsMan = new HashSet<>();
			Sportsman loggedSportsman = sportsmanRepository
					.findOne((Long) request.getSession().getAttribute("loggedId"));
			setOfSportsMan.add(loggedSportsman);
			training.setSportsmen(setOfSportsMan);
			training.setOwner(loggedSportsman);

			Set<Training> setOfTrainings = new HashSet<>();
			setOfTrainings.add(training);
			loggedSportsman.setTrainings(setOfTrainings);

			trainingRepository.save(training);

			return "redirect:/training/all";

		}
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model, HttpServletRequest request) {
		List<Training> loggedSportsmanTrainings = new ArrayList<>();
		List<Training> ownedTrainings = new ArrayList<>();
		List<Training> otherTrainings = new ArrayList<>();
				
		if (request.getSession() != null &&
				request.getSession().getAttribute("loggedin") != null &&
				request.getSession().getAttribute("loggedin").equals(true)) {
			Sportsman loggedSportsman = sportsmanRepository
					.findOne((Long) request.getSession().getAttribute("loggedId"));
			
			otherTrainings = trainingRepository.findTrainingsToJoin(loggedSportsman.getId());
			loggedSportsmanTrainings = trainingRepository.findJoinedTrainings(loggedSportsman.getId());
			ownedTrainings = trainingRepository.findOwnedTrainings(loggedSportsman.getId());



			model.addAttribute("sportsmanTrainings", loggedSportsmanTrainings);
			model.addAttribute("otherTrainings", otherTrainings);
			model.addAttribute("ownedTrainings", ownedTrainings);
		} else {
			return "training/all";
		}
		return "training/all";
	}

	@RequestMapping(value = "/join/{id}", method = RequestMethod.GET)
	public String joinTraining(@PathVariable("id") long id, ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		Training training = trainingRepository.findOne(id);
		Sportsman loggedSportsman = sportsmanRepository.findOne((Long) request.getSession().getAttribute("loggedId"));

		training.getSportsmen().add(loggedSportsman);
		loggedSportsman.getTrainings().add(training);

		trainingRepository.save(training);
			return "redirect:/training/all";
	}

	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public String findTrainingForm(ModelMap model) {
		List<Activity> listOfActivities = (List<Activity>) activityRepository.findAll();
		List<Place> listofPlaces = (List<Place>) placeRepository.findAll();
		model.addAttribute("activities", listOfActivities);
		model.addAttribute("places", listofPlaces);

		return "training/find";
	}

	@RequestMapping(value = "/find", method = RequestMethod.POST)
	public String findTraining(@RequestParam("activity") long activityId, @RequestParam("place") long placeId,
			ModelMap model, HttpServletRequest request) {
		List<Training> selectedTrainings = new ArrayList<>();
		List<Training> loggedSportsmanTrainings = new ArrayList<>();
		List<Training> otherTrainings = new ArrayList<>();
		List<Training> ownedTrainings = new ArrayList<>();

		if (request.getSession() != null &&
				request.getSession().getAttribute("loggedin") != null &&
				request.getSession().getAttribute("loggedin").equals(true)) {
			Sportsman loggedSportsman = sportsmanRepository.findOne((Long) request.getSession().getAttribute("loggedId"));

			selectedTrainings = trainingRepository.findByPlaceAndActivity(placeId, activityId);

			for (Training training : selectedTrainings) {
				if (loggedSportsman.getTrainings().contains(training)) {
					loggedSportsmanTrainings.add(training);
				} else {
					otherTrainings.add(training);
				}
			}
			
			for (Training training : selectedTrainings) {
				if(training.getOwner().equals(loggedSportsman)) {
					ownedTrainings.add(training);
					}
			}
			
			
			model.addAttribute("sportsmanTrainings", loggedSportsmanTrainings);
			model.addAttribute("otherTrainings", otherTrainings);
			model.addAttribute("ownedTrainings", ownedTrainings);

		}
		return "training/all";
	}


	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteTraining(@PathVariable("id") long id, HttpServletRequest request, ModelMap model) {
		Training training = trainingRepository.findOne(id);

		training.setActivity(null);
		training.setPlace(null);
		training.setSportsmen(null);
		training.setOwner(null);
		trainingRepository.save(training);
		trainingRepository.delete(training);

		model.addAttribute("msg", "Usunięto trening!");
		return "redirect:/training/all";
	}
	
	@RequestMapping(value = "/resign/{id}", method = RequestMethod.GET)
	public String resign(@PathVariable("id") long trainingId, ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		Training training = trainingRepository.findOne(trainingId);
		
		if (request.getSession().getAttribute("loggedin").equals(true)) {
			Sportsman loggedSportsman = sportsmanRepository.findOne((Long) request.getSession().getAttribute("loggedId"));
			training.getSportsmen().remove(loggedSportsman);
			loggedSportsman.getTrainings().remove(training);
			
			trainingRepository.save(training);
			model.addAttribute("msg", "Trening usunięto z Twojej listy!");
		}
		
		return "redirect:/training/all";
	}
	

}
