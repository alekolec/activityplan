package com.alekolec.activityPlan.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alekolec.activityPlan.entity.Activity;
import com.alekolec.activityPlan.entity.Place;
import com.alekolec.activityPlan.entity.Sportsman;
import com.alekolec.activityPlan.repository.SportsmanRepository;


@Controller
@RequestMapping("/sportsman")
public class SportsmanController {
	
	@Autowired
	SportsmanRepository sportsmanRepository;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model) {
		List<Sportsman> listOfSportsmen = (List<Sportsman>) sportsmanRepository.findAll();
		model.addAttribute("sportsmen", listOfSportsmen);
		return "sportsman/all";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addSportsmanView() {
		return "sportsman/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addSportsman(@RequestParam("login") String login,
							 @RequestParam("password") String password,
							 @RequestParam("name") String name,
							 @RequestParam("lastname") String lastname,
							 @RequestParam("year") int year, 
							 @RequestParam("month") int month,
							 @RequestParam("day") int day,
					         HttpServletRequest request, HttpServletResponse response, ModelMap model) throws IOException {
		String message;
		
		List<Sportsman> existingSportsman = (List<Sportsman>) sportsmanRepository.findAll();
		for(Sportsman sportsman : existingSportsman) {
			if(sportsman.getLogin().equals(login)) {
				message = "Taki użytkownik już istnieje!";
				model.addAttribute("msg", message);
				return "/sportsman/add";
			}
		}
		
		if(!Validator.validateBirthDate(year, month, day)) {
			message = "Podaj poprawną datę urodzin";
			model.addAttribute("msg", message);
			return "/sportsman/add";
			
		} else {
		Sportsman sportsman = new Sportsman();
		sportsman.setLogin(login);
		sportsman.setPassword(password);
		sportsman.setName(name);
		sportsman.setLastname(lastname);
		sportsman.setBirthDate(LocalDate.of(year, month, day));
		
		sportsmanRepository.save(sportsman);
		message = "Dodano użytkownika!";
		model.addAttribute("msg", message);
		return "index";
	}
	}
}
