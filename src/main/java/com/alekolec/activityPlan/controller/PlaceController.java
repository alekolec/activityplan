package com.alekolec.activityPlan.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alekolec.activityPlan.entity.Activity;
import com.alekolec.activityPlan.entity.Place;
import com.alekolec.activityPlan.repository.PlaceRepository;

@Controller
@RequestMapping("/place")
public class PlaceController {
	
	@Autowired
	PlaceRepository placeRepository;
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addPlaceView() {
		return "place/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addActivity(@RequestParam("name") String name, RedirectAttributes redirectAttrs)
			throws IOException {
		String message;
		List<Place> existingPlaces = (List<Place>) placeRepository.findAll();
		for(Place place :  existingPlaces) {
			if(place.getName().equals(name)) {
				message = "Takie miejsce już istnieje!";
				redirectAttrs.addFlashAttribute("msg", message);
				 return "redirect:/place/add";
			}
		}
		Place place = new Place();
		place.setName(name);

		placeRepository.save(place);
		message = "Dodano miejsce!";
		redirectAttrs.addFlashAttribute("msg", message);
		redirectAttrs.addFlashAttribute("newPlace", place);
		return "redirect:/place/all";
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model, @ModelAttribute("msg") String msg) {
		List<Place> listOfPlaces = (List<Place>) placeRepository.findAll();
		model.addAttribute("places", listOfPlaces);
		model.addAttribute("msg", msg);
		return "place/all";
	}

}
