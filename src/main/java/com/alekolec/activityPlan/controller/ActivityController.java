package com.alekolec.activityPlan.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alekolec.activityPlan.entity.Activity;
import com.alekolec.activityPlan.entity.Sportsman;
import com.alekolec.activityPlan.repository.ActivityRepository;

@Controller
@RequestMapping("/activity")
public class ActivityController {

	@Autowired
	ActivityRepository activityRepository;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addActivityView() {
		return "activity/add";
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model, @ModelAttribute("msg") String msg) {
		List<Activity> listOfActivities = (List<Activity>) activityRepository.findAll();
		model.addAttribute("activities", listOfActivities);
		model.addAttribute("msg", msg);
		return "activity/all";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addActivity(@RequestParam("name") String name, ModelMap model, RedirectAttributes redirectAttrs)
			throws IOException {
		List<Activity> existingActivities = (List<Activity>) activityRepository.findAll();
		for(Activity tempActivity : existingActivities) {
			if(name.equals(tempActivity.getName())) {
				redirectAttrs.addFlashAttribute("msg", "Taka aktywność już istnieje!");
				return "redirect:/activity/add";
			}
		}
		Activity activity = new Activity();
		activity.setName(name);

		activityRepository.save(activity);

		redirectAttrs.addFlashAttribute("msg", "Dodano aktywność!");
		redirectAttrs.addFlashAttribute("newActivity", activity);
		return "redirect:/activity/all";
	}
}
