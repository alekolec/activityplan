package com.alekolec.activityPlan.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HomeController {
	
	@RequestMapping("/")
	public String homepage(HttpServletRequest request) {
		request.getSession().setAttribute("loggedin", false);
			return "index";
		
	}
	
	

}
