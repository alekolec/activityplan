package com.alekolec.activityPlan.controller;

import java.time.LocalDateTime;

public class Validator {
	
	public static boolean validateDate(int year, int month, int day) {
		LocalDateTime now = LocalDateTime.now();
		
		if(year > now.getYear() + 10) {
			return false;
		}
		
		if(1 > month || month > 12) {
			return false;
		}
		
		if(1 > day || day > 31) {
			return false;
		}
		
		if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
			if(day > 30) {
				return false;
			}
		}
						
		if(now.getYear() > year) {
			return false;
		}
		
		if(now.getYear() == year) {
		if(now.getMonthValue() > month) {
			return false;
			}
		}
		
		if(now.getMonthValue() == month) {
			if(now.getDayOfMonth() > day) {
				return false;
			}
		}
		
		return true;
	}
	
	public static boolean validateTime(int hour, int minute) {
		if(1 > hour || hour > 23) {
			return false;
		}
		
		if(0 > minute || minute > 60) {
			return false;
		}
		
		return true;
	}
	
	public static boolean validateBirthDate(int year, int month, int day) {
		LocalDateTime now = LocalDateTime.now();
		if(year < now.getYear() - 100 || year >= now.getYear()) {
			return false;
		}
		
		if(1 > month || month > 12) {
			return false;
		}
		
		if(1 > day || day > 31) {
			return false;
		}
		
		if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
			if(day > 30) {
				return false;
			}
		}
		return true;
	}

}
