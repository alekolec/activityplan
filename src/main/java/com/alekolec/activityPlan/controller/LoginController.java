package com.alekolec.activityPlan.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alekolec.activityPlan.entity.Sportsman;
import com.alekolec.activityPlan.repository.SportsmanRepository;

@Controller
public class LoginController {
	
	@Autowired
	SportsmanRepository sportsmanRepository;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginView() {
		return "log";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestParam("login") String login,
						@RequestParam("password") String password,
						HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		
		for(Sportsman sportsman : sportsmanRepository.findAll()) {
			if(sportsman.getLogin().equals(login) && sportsman.getPassword().equals(password)) {
				request.getSession().setAttribute("loggedin", true);
				request.getSession().setAttribute("loggedId", sportsman.getId());
				request.getSession().setAttribute("loggedLogin", sportsman.getLogin());
				model.addAttribute("msg", "Zalogowano!");
				return "index";
							
			}
		}
			model.addAttribute("msg", "Z�y login lub has�o!");
		return "log";
		
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, ModelMap model) {
		request.getSession().setAttribute("loggedin", false);
		model.addAttribute("msg", "Wylogowano!");
		return "index";
	}

}
