<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
<div class="well">
		<form action='<c:url value="/sportsman/add" />' method="POST">
			<c:if test="${msg ne null}">
			<p>${msg}</p>
		</c:if>
		
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Login</span>
	  <input type="text" name="login" class="form-control" placeholder="Podaj login" aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Hasło</span>
	  <input type="password" name="password" class="form-control" placeholder="Podaj hasło" aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Imię</span>
	  <input type="text" name="name" class="form-control" placeholder="Podaj imię" aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Nazwisko</span>
	  <input type="text" name="lastname" class="form-control" placeholder="Podaj nazwisko" aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Data urodzenia</span>
	  <input type="text" name="year" class="form-control" placeholder="Podaj rok" aria-describedby="basic-addon1">
	  <input type="text" name="month" class="form-control" placeholder="Podaj miesiąc" aria-describedby="basic-addon1">
	  <input type="text" name="day" class="form-control" placeholder="Podaj dzień" aria-describedby="basic-addon1">
	</div>
	
	<button type="submit" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Dodaj</button>
</form>
		
</div>			
			
<c:import url="../footer.jsp" />