<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
<div class="well">
		<c:forEach items="${sportsmen}" var="sportsman">
		<p>${sportsman.name} ${sportsman.lastname}</p>
		</c:forEach>
		
</div>			
			
<c:import url="../footer.jsp" />