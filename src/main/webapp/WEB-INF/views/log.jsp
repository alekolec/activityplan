<%@ page language="java" pageEncoding="UTF-8" 
		 contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:import url="header.jsp" />
<div class = "well">
<form action='<c:url value="/login" />' method="POST">
<c:if test="${msg ne null}">
			<p>${msg}</p>
		</c:if>
	<input type="text" name="login" placeholder="Podaj login" />
	<input type="password" name="password" placeholder="Podaj hasło"/>
	<button type="submit" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Zaloguj</button>
</form>
</div>
<c:import url="footer.jsp" />