<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
<div class="well">
		<form action='<c:url value="/place/add" />' method="POST">
		<c:if test="${msg ne null}">
			<p>${msg}</p>
		</c:if>
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Nazwa</span>
	  <input type="text" name="name" class="form-control" placeholder="Podaj nazwę" aria-describedby="basic-addon1">
	</div>
	

	<button type="submit" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Dodaj</button>
</form>
		
</div>			
			
<c:import url="../footer.jsp" />