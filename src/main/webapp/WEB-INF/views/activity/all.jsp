<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
<div class="well">
<c:if test="${msg ne null}">
			<p style="text-align: center; color: #088fd2; font-size: 16px"><b>${msg}</b></p>
		</c:if>
		<c:forEach items="${activities}" var="activity">
		
               <c:if test="${activity eq newActivity }">
                   <c:set var="checked" value="style='color: #088fd2'"/>
                   </c:if>
           
		<p ${checked} >${activity.name}</p>
		</c:forEach>
		
</div>		
			
<c:import url="../footer.jsp" />