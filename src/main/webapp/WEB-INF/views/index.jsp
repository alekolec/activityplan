<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="header.jsp" />
<div class="well">
<c:choose>
		
		<c:when test="${msg ne null}">
			<p>${msg}</p>
		</c:when>
		<c:otherwise>
		<h2>Przedstawiamy ActivityPlan! </h2>
		</c:otherwise>
		</c:choose>
		
</div>			
			
<c:import url="footer.jsp" />