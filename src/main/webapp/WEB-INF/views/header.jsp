<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ActivityPlan</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<style>
		.input-group-addon, .input-group-btn {
		min-width: 100px;
		}
		.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
		background-color: #000000 !important;
    	color: white;
		}
		.nav > li > a:hover{
   		 background-color:#000000;
		}
		/*.label{
	   display:inline-block;
	   float:right;
	   margin-top:10px;
	    } */
		</style>
	</head>
	<body>
	<div class="container">
	<div class="page-header">
		<h1>ACTIVITY<small> plan <img width="100" height="100" src="http://web1.westfields-h.schools.nsw.edu.au/moodle/pluginfile.php/11791/coursecat/description/2.jpg" /></small></h1>
		</div>
			<nav class="navbar" style="background-image: linear-gradient(to bottom,#094d6f 0,#333333 100%)">
			<div class="col-md-10">
				<a href='<c:url value="/" />' class="navbar-brand" style="color: white">ActivityPlan</a>
				<ul class="nav navbar-nav">
						
				<c:if test="${sessionScope.loggedin}">	
				<div class="btn-group" style="margin-top: 7px">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Aktywności <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href='<c:url value="/activity/add" />' >Dodaj aktywność</a></li>
				   <li><a href='<c:url value="/activity/all" />'>Wyświetl aktywności</a></li>
				    </ul>
				</div>
				
				<div class="btn-group" style="margin-top: 7px">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Miejsca <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href='<c:url value="/place/add" />'>Dodaj miejsce</a></li>
				    <li><a href='<c:url value="/place/all" />'>Wyświetl miejsca</a></li>
				    </ul>
				</div>
				
				<div class="btn-group" style="margin-top: 7px">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Treningi <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href='<c:url value="/training/add" />'>Dodaj trening</a></li>
				<li><a href='<c:url value="/training/all" />'>Wyświetl treningi</a></li>
				<li><a href='<c:url value="/training/find" />'>Szukaj treningu</a></li>
				    </ul>
				</div>
												
				
				<button type="button" class="btn btn-default" style="margin-top: 7px">
				<a href='<c:url value="/logout" />' style="color: black">Wyloguj</a></button>
				<!-- span class="label label-default" style="font-size: 20px; line-height: 20px">Witaj, ${sessionScope.loggedLogin}</span -->
				</c:if>
				<c:if test="${sessionScope.loggedin ne true}">
				<button type="button" class="btn btn-default" style="margin-top: 7px">
				<a href='<c:url value="/login" />' style="color: black">Zaloguj</a> </button>
				
				<button type="button" class="btn btn-default" style="margin-top: 7px">
				<a href='<c:url value="/sportsman/add" />' style="color: black">Zarejestruj</a></button>
				</c:if>
										
				</ul>
				</div>
				<div class="col-md-2">
				<c:if test="${sessionScope.loggedin}">	
				<span class="label label-default" style="font-size: 20px; line-height: 50px">Witaj, ${sessionScope.loggedLogin}</span>
				</c:if>
				</div>
			</nav>