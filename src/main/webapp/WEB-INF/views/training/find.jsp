<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />

<div class="well">
	<form action='<c:url value="/training/find" />' method="POST">
		
	<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Sport</span>

		<select class="selectpicker" name="activity"  class="form-control">
		
			<c:forEach items="${activities}" var="activity">
			<option value="${activity.id}">${activity.name}</option>
			</c:forEach>
	
		</select>
	</div>
	
	<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Miejsce</span>

		<select class="selectpicker" name="place"  class="form-control">
		
			<c:forEach items="${places}" var="place">
			<option value="${place.id}">${place.name}</option>
			</c:forEach>
	
		</select>
	</div>
	
	<button type="submit" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Szukaj</button>
	</form>
		
</div>			
			
<c:import url="../footer.jsp" />