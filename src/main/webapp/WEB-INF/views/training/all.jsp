<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />

<div class="well">
<c:choose>
	<c:when test="${sessionScope.loggedin ne true}">
		<div class="alert alert-danger">
			<strong>Błąd</strong>
			Musisz najpierw się zalogować, aby wyświetlić treningi
		</div>
	</c:when>
	<c:otherwise>
	<div class="well">
	<h2>Treningi, do których możesz dołączyć: </h2>
	<table class="table">
	<thead>
		<tr>
			<th>Trening</th>
			<th>Miejsce</th>
			<th>Data</th>
			<th>Akcja</th>
		</tr>
	</thead>
	<tbody>
		
		<c:forEach items="${otherTrainings}" var="training">
		<tr>
		<td>${training.activity.name}</td>
		<td>${training.place.name}</td>
		<td> ${training.date.dayOfMonth} - ${training.date.month} - ${training.date.year}  </td>
		<c:if test="${sessionScope.loggedin}">
		<td><a href="<c:url value='/training/join/${training.id}' />" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Dołącz</a></td>
		</c:if>
		</c:forEach>
		</tbody>
		</table>
		</div>
		
		<div class = "well">
		<h2>Treningi, do których dołączyłeś </h2>
		<table class="table">
		<thead>
		<tr>
			<th>Trening</th>
			<th>Miejsce</th>
			<th>Data</th>
			<th>Akcja</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${sportsmanTrainings}" var="mytraining">
		<tr>
		<td>${mytraining.activity.name} </td>
		<td>${mytraining.place.name} </td>
		<td> ${mytraining.date.dayOfMonth} - ${mytraining.date.month} - ${mytraining.date.year} </td>
		<td><a href="<c:url value='/training/resign/${mytraining.id}' />" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Zrezygnuj</a></td>
		</c:forEach>
		</tbody>
		</table>
		</div>
		
		<div class = "well">
		<h2>Twoje treningi </h2>
		<table class="table">
		<thead>
		<tr>
			<th>Trening</th>
			<th>Miejsce</th>
			<th>Data</th>
			<th>Akcja</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${ownedTrainings}" var="owntraining">
		<tr>
		<td>${owntraining.activity.name} </td>
		<td>${owntraining.place.name} </td>
		<td> ${owntraining.date.dayOfMonth} - ${owntraining.date.month} - ${owntraining.date.year} </td>
		<td> <a href="<c:url value='/training/delete/${owntraining.id}' />" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Usuń</a></td>
		</c:forEach>
		</tbody>
		</table>
		</div>
		
		
</c:otherwise>
</c:choose>	
</div>
		
		
			
<c:import url="../footer.jsp" />