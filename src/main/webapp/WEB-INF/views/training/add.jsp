<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
<div class="well">
<c:choose>
	<c:when test="${sessionScope.loggedin ne true}">
		<div class="alert alert-danger">
			<strong>Błąd</strong>
			Musisz najpierw się zalogować, aby dodać trening.
		</div>
	</c:when>
	<c:otherwise>
	<form action='<c:url value="/training/add" />' method="POST">
	
		<c:if test="${msg ne null}">
			<p>${msg}</p>
		</c:if>
		
	<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Sport</span>

	<select class="selectpicker" name="activity"  class="form-control">
	
		<c:forEach items="${activities}" var="activity">
		<option value="${activity.id}">${activity.name}</option>
		</c:forEach>

</select>
</div>

	<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Miejsce</span>

	<select class="selectpicker" name="place"  class="form-control">
	
		<c:forEach items="${places}" var="place">
		<option value="${place.id}">${place.name}</option>
		</c:forEach>

</select>
</div>

 	<span class="label label-default">${loggedSportsman.login}</span>

		<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Rok</span>

	<select class="selectpicker" name="year"  class="form-control">
	
		<c:forEach items="${years}" var="year">
		<option value="${year}">${year}</option>
		</c:forEach>

</select>
</div>
	
	<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Miesiąc</span>

	<select class="selectpicker" name="month"  class="form-control">
	
		<c:forEach items="${months}" var="month">
		<option value="${month}">${month}</option>
		</c:forEach>

</select>
</div>
	
	<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Dzień</span>

	<select class="selectpicker" name="day"  class="form-control">
	
		<c:forEach items="${days}" var="day">
		<option value="${day}">${day}</option>
		</c:forEach>

</select>
</div>
	
	<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Godzina</span>

	<select class="selectpicker" name="hour"  class="form-control">
	
		<c:forEach items="${hours}" var="hour">
		<option value="${hour}">${hour}</option>
		</c:forEach>

</select>
</div>
	
		<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Minuty</span>

	<select class="selectpicker" name="minute"  class="form-control">
	
		<c:forEach items="${minutes}" var="minute">
		<option value="${minute}">${minute}</option>
		</c:forEach>

</select>
</div>
	

	<button type="submit" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Dodaj</button>

</form>
</c:otherwise>
</c:choose>
		
</div>			
			
<c:import url="../footer.jsp" />